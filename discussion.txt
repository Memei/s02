CLI Commands
mkdir - create a new folder/directory
	syntax:
		mkdir <folderName>
		ex. mkdir batch-197
touch - used to create files
	syntax:
		touch <fileName>
		ex. touch discussion.txt
cd - change directory
	- change current directory/folder that we are currently working on with our CLI (gitbash)
	syntax:
		cd <folderName/path to folder>
		ex. cd Documents
cd .. - it moves back directory/folder
	syntax:
		cd ..
ls - list
	- it list files and folders contained by the current directory
	syntax:
		ls
pwd - present working directory
	- to identify current directory	we are working on
	syntax:
		pwd	
When using git for the first time in you rcomputer/machine

1. configure our Git
	- git config --global user.email "<emailFromGitlab>""
	- this will allow us to identify the account from gitlab/github who will push/upload files into out online Gitlab or Github services

	- git config --global user.name "<nameUsedInGitlab>"
	- this will allow us to identify the name of the user who is trying to upload/push files into our online Gitlab or Github services.
	- this allow us to determins the name of the person who uploaded the latest commit/version in our repo

What is SSH key?
	- SSH or Secure Shell Key
		- tools used to authenticate the uploading of or other task when manipulating or using git repositories
	- allows us to push or upload into our online git repo without the use of passwords

	ssh-keygen -t ed25519
		- t option to select algorithm
		- ed25519 is the new algo added in OpenSSH


git commands - pushing/uploading:

===
Name: Ruby Mae
Address: Progressive Gonzaga Cagayan
Hobbies:
	1. Reading
	2. Playing Games
	3. Coding
	4. Sleeping
